project_menu
============

Project to learn how to program on Nintendo DS from October 2011. Would love to dive back into this one day.

There are tiny little programs that are loaded by a "menu" of sorts. Can be navigated with the buttons on the Nintendo DS. Little games, tests of NDS functionality, and I think a BMP decoder on one of them. 

You need the R4DS development files to make this all work. 

TODO: figure out where I bought this from. It was some store in France.
TODO: which website did I download the R4DS software from? I don't think it came with the kit.
