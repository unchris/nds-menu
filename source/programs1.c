#ifndef __NDS_HEADER__
#define __NDS_HEADER__
#include <nds.h>
#include <stdio.h>
#endif

#include "programs1.h"

/*
	This program paints the bottom DS screen with colours.
	The Top DS Screen will print numbered lines of "CC owns DS!"
*/
void programs1_1(void)
	{
	int i;

	consoleDemoInit();

	videoSetMode(MODE_FB0);

	vramSetBankA(VRAM_A_LCD);

	for (i = 0; i < 25; ++i)
		{
		printf("%i. CC owns DS!\n", i + 1);
		}

	i = 0;
	//The bottom DS Screen is 256 * 192
	for (i = 0; i < 256 * 192; ++i)
		{
		VRAM_A[i] = RGB15(i % 31,31 - i % 31,0);
		}
		
	while(TRUE)
		{
		swiWaitForVBlank();
		if (checkExit(TRUE))
			{
			break;
			}
		}
	}
	
/*
	This program tells you whether the A button is pressed or
	released by printing to the bottom screen.
*/
void programs1_2(void)
	{
	consoleDemoInit();
 
	while(TRUE)
		{
		iprintf("Press buttons!\n");
		//note: it seems that the DS can only register 
		//      two of A,B,L,R at the same time
		
		//A,B,L,R
		if (!(REG_KEYINPUT & KEY_A))
			{
			printf("Button A is pressed.\n");
			}
		if (!(REG_KEYINPUT & KEY_B))
			{
			printf("Button B is pressed.\n");
			}
		if (!(REG_KEYINPUT & KEY_L))
			{
			printf("Button L is pressed.\n");
			}
		if (!(REG_KEYINPUT & KEY_R))
			{
			printf("Button R is pressed.\n");
			}
			
		
		//LEFT, RIGHT, DOWN, UP
		/*
			Interesting Notes:
			When you hold LEFT+RIGHT, 			neither is registered
			When you hold UP+DOWN, 				neither is registered
			When you hold LEFT+UP then DOWN, 	LEFT+UP is registered
			When you hold RIGHT+UP then DOWN, 	RIGHT+UP is registered
			When you hold RIGHT+DOWN then LEFT, DOWN is registered
			When you hold LEFT+DOWN then RIGHT, DOWN is registered
			When you hold RIGHT then UP+DOWN, 	RIGHT is registered
			When you hold RIGHT then DOWN+UP, 	RIGHT is registered
			When you hold LEFT then UP+DOWN		LEFT+UP is registered
			When you hold LEFT then DOWN+UP		LEFT+DOWN is registered
			.. there are probably other quirks...
		*/
		if (!(REG_KEYINPUT & KEY_LEFT))
			{
			printf("Button LEFT is pressed.\n");
			}
		if (!(REG_KEYINPUT & KEY_RIGHT))
			{
			printf("Button RIGHT is pressed.\n");
			}
		if (!(REG_KEYINPUT & KEY_DOWN))
			{
			printf("Button DOWN is pressed.\n");
			}
		if (!(REG_KEYINPUT & KEY_UP))
			{
			printf("Button UP is pressed.\n");
			}
			
	    //START, SELECT
		/*
			Note:
			When you hold START+SELECT and vice-versa: then (DOWN or RIGHT or LEFT) 
				only START+SELECT is registered
			When you hold START+SELECT and vice-versa, then UP:
				START+SELECT+UP is registered
				
			START+SELECT+L+R: shuts the system off!
			
			START+SELECT+L works
			START+SELECT+R works
			START+SELECT+L+UP works
			START+SELECT+R+UP works
			none of the other START+SELECT+(R or L)+DIRECTION works
			
			START+SELECT then A,B,A+B: only START+SELECT
		*/
		
		if (!(REG_KEYINPUT & KEY_SELECT))
			{
			printf("Button SELECT is pressed.\n");
			}
		if (!(REG_KEYINPUT & KEY_START))
			{
			printf("Button START is pressed.\n");
			}
 
		swiWaitForVBlank();
 
		//without this next line, the text will continue to print
		// over and over and over again from where it last was.
		//consoleClear() is essentially like CLS in dos
		consoleClear();
		if (checkExit(TRUE))
			break;
		}
 
	}

/*
	This program also reads the button states, but using the libnds
	wrapper that lets us have access to X,Y,Lid,Touch since they
	are on the ARM7 and not on the ARM9 like REG_KEYINPUT is
*/
void programs1_3(void)
	{
	consoleDemoInit();
 
	while(TRUE)
		{
		iprintf("Press buttons!\n");
		scanKeys();
		int held = keysHeld();
 
		if( held & KEY_A)
			printf("Key A is pressed\n");
		else
			printf("Key A is released\n");
 
		if( held & KEY_X)
			printf("Key X is pressed\n");
		else
			printf("Key X is released\n");
 
		if( held & KEY_TOUCH)
			printf("Touch pad is touched\n");
		else
			printf("Touch pad is not touched\n");
 
		swiWaitForVBlank();
 
		consoleClear();
		if (checkExit(FALSE))
			break;
		}
	
	}
	

/*
	This program is a demo on how to use the frame buffered mode
	to draw graphics
*/
void programs1_4(void)
	{
	int i;
 
	//initialize the DS Dos-like functionality
	consoleDemoInit();
	iprintf("A = green, X = blue\n");
 
	//set frame buffer mode 0
	videoSetMode(MODE_FB0);
 
	//enable VRAM A for writing by the cpu and use 
	//as a framebuffer by video hardware
	//VRAM A is one of the blocks of memory available for graphics ram
	vramSetBankA(VRAM_A_LCD);
 
	while(TRUE)
		{
		u16 color = RGB15(31,0,0); //red
 
		scanKeys();
		int held = keysHeld();
 
		//obviously in the scheme below, X will take precedent if both keys are held
		if(held & KEY_A)
			color = RGB15(0,31,0); //green
		
		if (held & KEY_X)
			color = RGB15(0,0,31); //blue
 
		swiWaitForVBlank();
 
		//fill video memory with the chosen color
		for(i = 0; i < 256*192; i++)
			VRAM_A[i] = color;
		
		
		if (checkExit(FALSE))
			{
			break;
			}
		}
 
	}