#ifndef __MY_MAIN_H__
#define __MY_MAIN_H__

#ifndef __NDS_HEADER__
#define __NDS_HEADER__
#include <nds.h>
#include <stdio.h>
#endif

bool checkExit(bool scan)
	{
	if (scan)
		scanKeys();
	return (keysHeld() & KEY_L) && (keysHeld() & KEY_R);
	}
	
void ClearScreen(void)
{
     int i;
     
     for(i = 0; i < 256 * 192; i++)
           VRAM_A[i] = RGB15(0,0,0);
}
	
	
#endif