#ifndef __WAV_H__
#define __WAV_H__


typedef struct
{
	char ChunkID[4]; 	// "RIFF"
    u32 ChunkSize; 		// number of bytes left in the file after first 8
	char Format[4]; 	// "WAVE"

}__attribute__ ((packed)) WavHead;


typedef struct
{
	char Subchunk1ID[4];	// "fmt "
	u32 Subchunk1Size;		// 16 for PCM, which is size of rest of subchunk
	u16 AudioFormat;		// PCM = 1 (i.e., Linear quantization)
							// values other than one indicate other compression
	u16 NumChannels;		// Mono = 1, Stereo = 2, etc
	u32 SampleRate;			// 8000, 44100, etc
	u32 ByteRate;			// == SampleRate * NumChannels * BitsPerSample/8
	u16 BlockAlign;			// == NumChannels * BitsPerSample/8
							// The number of bytes for one sample including
							// all channels. 
	u16 BitsPerSample;		// 8 bits = 8, 16 bits = 16, etc
}__attribute__ ((packed)) WavFmt;

typedef struct
{
	char DataChunkID[4];	// "data"
	u32 DataChunkSize; 		// == NumSamples * NumChannels * BitsPerSample/8
							// basically the number of bytes left following DataChunkSize
	
}__attribute__ ((packed)) WavData;

typedef struct
{
	WavHead header;
	WavFmt fmt;
	WavData data;
	

}__attribute__ ((packed)) WavHeader;

typedef struct
{
	s16 *samples;
	u32 cursor;
}__attribute__ ((packed)) WavData16PCM;

typedef struct
{
	s8 *samples;
	u32 cursor;
}__attribute__ ((packed)) WavData8PCM;


#endif