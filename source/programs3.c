#ifndef __NDS_HEADER__
#define __NDS_HEADER__
#include <nds.h>
#include <stdio.h>
#endif

#include "programs3.h"
 
void programs3_run(void)
	{
	touchPosition touch;
 
	videoSetMode(MODE_FB0);
	vramSetBankA(VRAM_A_LCD);
        
	//notice we make sure the main graphics engine renders
	//to the lower lcd screen as it would be hard to draw if the 
	//pixels did not show up directly beneath the pen
	consoleClear();
	iprintf("Use the mouse/pen to draw on \nbottom screen\n");
	lcdMainOnBottom();
 
	while(TRUE)
		{
		scanKeys();
 
		if(keysHeld() & KEY_TOUCH)
			{
			// write the touchscreen coordinates in the touch variable
			touchRead(&touch);
			VRAM_A[touch.px + touch.py * SCREEN_WIDTH] = rand();
			}
			
		if (checkExit(FALSE))
			{
			break;
			}
		}
		
	lcdMainOnTop();
 
		
	}