#ifndef __NDS_HEADER__
#define __NDS_HEADER__
#include <nds.h>
#include <stdio.h>

#endif

#include "programs4.h"
#include <fat.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include "wav.h"

 
 
//------------------------------------------------------------------------------ 
// Program 1 shows how to draw to the top screen using a Mode 5 background.
// I believe it's the Mode 5 2D Extended background (Layer 3, according to
// the bgInit() function later)
//
// Interestingly, there is a lot of screen tearing, indicating that regenerating 
// the BG_GFX array is actually time-consuming. 
//
// A later program will show how to avoid this (I hope) using double-buffering
//------------------------------------------------------------------------------
void programs4_1(void)
	{
	int i;
	
	//set video mode to mode 5
	videoSetMode(MODE_5_2D);	
	
	//map vram a to start of background graphics memory
	vramSetBankA(VRAM_A_MAIN_BG);
	
	//initialize the background
	bgInit(3, BgType_Bmp16, BgSize_B16_256x256, 0,0);
	
	//write the color red into the background
	consoleClear();
	while(1) 
		{
		swiWaitForVBlank();
		for(i=0; i<256*256; i++)
			{
			if (rand() % 100 > 25)
				BG_GFX[i]=RGB15(31,0,0) | BIT(15);
			else
				BG_GFX[i]=RGB15(31,0,0);
			}
		if (checkExit(TRUE))
			{
			ClearScreen();
			break;
			}
		}
	}
 
//------------------------------------------------------------------------------
// Program 2
// Does some of the things from program1 more manually as well...
// shows sub-display programming
// bgInit() is replaced with direct access to the BACKGROUND structure
//------------------------------------------------------------------------------ 

void programs4_2(void)
	{
	int i;
	//point our video buffer to the start of bitmap background video
	u16* video_buffer_main = (u16*)BG_BMP_RAM(0);
	u16* video_buffer_sub =  (u16*)BG_BMP_RAM_SUB(0);
	
	//make sure there's nothing written there
	consoleClear();
 
	//set video mode to mode 5 with background 3 enabled
	videoSetMode(MODE_5_2D | DISPLAY_BG3_ACTIVE);	
	videoSetModeSub(MODE_5_2D | DISPLAY_BG3_ACTIVE);
 
	//map vram a to start of main background graphics memory
	//CC Note: 	0600:0000 starts main background 512k max
	//		   	0620:0000 starts sub background 128k max
	//			0640:0000 starts main sprite 256k max
	//			0660:0000 starts sub sprite 128k max
	vramSetBankA(VRAM_A_MAIN_BG_0x06000000);
	vramSetBankC(VRAM_C_SUB_BG_0x06200000);
 
	/* CC note, BACKGROUND struct used instead of bgInit */
	//initialize the background
	//i wonder if [3] refers to layer 3?
	BACKGROUND.control[3] = BG_BMP16_256x256 | BG_BMP_BASE(0);
	
	BACKGROUND.bg3_rotation.hdy = 0;
	BACKGROUND.bg3_rotation.hdx = 1 << 8;
	BACKGROUND.bg3_rotation.vdx = 0;
	BACKGROUND.bg3_rotation.vdy = 1 << 8;
 
	//initialize the sub background
	BACKGROUND_SUB.control[3] = BG_BMP16_256x256 | BG_BMP_BASE(0);
	
	BACKGROUND_SUB.bg3_rotation.hdy = 0;
	BACKGROUND_SUB.bg3_rotation.hdx = 1 << 8; //seriously, why not just 256??
	BACKGROUND_SUB.bg3_rotation.vdx = 0;
	BACKGROUND_SUB.bg3_rotation.vdy = 1 << 8;
 
	//paint the main screen red
	for(i = 0; i < 256 * 256; i++)
		video_buffer_main[i] = RGB15(31,0,0) | BIT(15);
	
 	//paint the sub screen blue
	for(i = 0; i < 256 * 256; i++)
		video_buffer_sub[i] = RGB15(0,0,31) | BIT(15);
		
	while(1) 
		{
		swiWaitForVBlank();
		if (checkExit(TRUE))
			{
			//ClearScreen();
			break;
			}
		}
	}
	
	
//------------------------------------------------------------------------------
// Program 3
// how to read in a 256-colour bitmap with paletted colours
//------------------------------------------------------------------------------ 
void programs4_3(void)
	{
	BmpFile* bmp = (BmpFile*)beerguy_bin;
	
	consoleDemoInit();
	
	//point our video buffer to the start of bitmap background video
	u16* video_buffer_main = (u16*)BG_BMP_RAM(0);
	//set video mode to mode 5 with background 3 enabled
	videoSetMode(MODE_5_2D | DISPLAY_BG3_ACTIVE);	
	//map vram a to start of main background graphics memory
	vramSetBankA(VRAM_A_MAIN_BG_0x06000000);
	//initialize the background
	BACKGROUND.control[3] = BG_BMP16_256x256 | BG_BMP_BASE(0);
	BACKGROUND.bg3_rotation.hdy = 0;
	BACKGROUND.bg3_rotation.hdx = 256;
	BACKGROUND.bg3_rotation.vdx = 0;
	BACKGROUND.bg3_rotation.vdy = 256;
	
	int i;
	for (i = 0; i < 256; ++i)
		{
		//because paint uses 8-bit, we use 5-bit
		BG_PALETTE[i] = RGB15(bmp->colors[i].red >> 3, 
							  bmp->colors[i].green >> 3, 
							  bmp->colors[i].blue >> 3) | BIT(15);
		}
	
	int x,y,yback,screen_pixel,image_pixel;
	for (y = 0; y < bmp->info.height; ++y)
		{
		yback = bmp->info.height - 1 - y;
		for (x = 0; x < bmp->info.width; ++x)
			{
			screen_pixel = x + (y * 256);
			image_pixel = (yback * 256) + x;
			video_buffer_main[screen_pixel] = BG_PALETTE[bmp->image[image_pixel]];
			}
		}
	
	while(1)
		{
		swiWaitForVBlank();
		consoleClear();
		printf("%c%c\n", bmp->header.signature[0], bmp->header.signature[1]);
		printf("bit depth: %i\n", bmp->info.bitDepth);
		printf("width:     %i\n", bmp->info.width);
		printf("height:    %i\n", bmp->info.height);
		if (checkExit(TRUE))
			{
			ClearScreen();
			break;
			}
		}

	}
 
 
 
 
 
 
//------------------------------------------------------------------------------
// Program 4
// Simple tiled backgrounds
//------------------------------------------------------------------------------ 

//1 and 2 in the redTile and greenTile variables refers to an 8-bit palette
//index in BG_PALETTE
//so they are u8 (unsigned 8-bit int) since that is the format of 
//BG_Palette indices; you can only have 256 colours in BG_PALETTE to index

void assimilatePalette(BmpFile* bmp,int offset)
	{
	//so, bmps are annoying, and include 256 colours even when there are less
	//we just want to append the used colours into the BG_PALETTE and 
	//rewrite the image's PALETTE numbers
	u16 palette[256];
	int i;
	for (i = 0; i < 256; ++i)
		{
		//because paint uses 8-bit, we use 5-bit
		palette[i] = RGB15(bmp->colors[i].red >> 3, 
							bmp->colors[i].green >> 3, 
							bmp->colors[i].blue >> 3) | BIT(15);
		}
	u16 map[256];
	for (i = 0; i < 256; ++i)
		{
		map[i] = 512;
		}
	
	int x,y;
	u8 next = offset;
	for (y = 0; y < bmp->info.height; ++y)
		{
		for (x = 0; x < bmp->info.width; ++ x)
			{
			int impix = x + y*bmp->info.width;
			u8 index = bmp->image[impix];
			if (map[index] == 512)
				{
				//map bmp palette index to new index
				map[index] = next;
				//set BG_PALETTE to value at old palette index
				BG_PALETTE[next] = palette[index];
				//replace image's palette index with new one
				bmp->image[impix] = next;
				//increase the new index counter
				++next;
				}
			else
				{
				//replace image's palette index with new one
				bmp->image[impix] = (u8)map[index];
				}
			
				
			}
		}
	}
	
	
void programs4_4(void)
	{
	
	consoleClear();
	int i;
	
	 u8 redTile[64] = 
		{
		1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1
		};
	 
	u8 greenTile[64] = 
		{
		2,2,2,2,2,2,2,2,
		2,2,2,2,2,2,2,2,
		2,2,2,2,2,2,2,2,
		2,2,2,2,2,2,2,2,
		2,2,2,2,2,2,2,2,
		2,2,2,2,2,2,2,2,
		2,2,2,2,2,2,2,2,
		2,2,2,2,2,2,2,2
		};

	u8 blueTile[64] = 
		{
		3,3,3,3,3,3,3,3,
		3,3,3,3,3,3,3,3,
		3,3,3,3,3,3,3,3,
		3,3,3,3,3,3,3,3,
		3,3,3,3,3,3,3,3,
		3,3,3,3,3,3,3,3,
		3,3,3,3,3,3,3,3,
		3,3,3,3,3,3,3,3
		};

	u8 bmpTile[64];

	//u16 mapfile[32*32]; //what the hell was this for again?
	
	//set video mode and map vram to the background
	videoSetMode(MODE_0_2D | DISPLAY_BG0_ACTIVE);
	vramSetBankA(VRAM_A_MAIN_BG_0x06000000);
 
	//get the address of the tile and map blocks 
	u8* tileMemory = (u8*)BG_TILE_RAM(1);
	u16* mapMemory = (u16*)BG_MAP_RAM(0);
	
	//tell the DS where we are putting everything and set 256 color mode and that we are using a 32 by 32 tile map.
	REG_BG0CNT = BG_32x32 | BG_COLOR_256 | BG_MAP_BASE(0) | BG_TILE_BASE(1);
 
	//load our palette
	BG_PALETTE[1] = RGB15(31,0,0);
	BG_PALETTE[2] = RGB15(0,31,0);
	BG_PALETTE[3] = RGB15(0,0,31);
	
	//load image
	BmpFile* bmp = (BmpFile*)beerguy_bin;
	assimilatePalette(bmp, 4);
	
  
	//copy the tiles into tile memory one after the other
	memcpy(tileMemory, redTile, 64);
	memcpy(tileMemory+64, greenTile, 64);
	memcpy(tileMemory+64*2, blueTile, 64);
	memcpy(tileMemory+64*3, bmpTile, 64);
	
	//create a map in map memory
	for(i = 0; i < 32 * 32; i++)
		mapMemory[i] = rand() % 5;
		
	while(1)
		{
		swiWaitForVBlank();
		if (checkExit(TRUE))
			{
			ClearScreen();
			break;
			}
		}
	
	}

//------------------------------------------------------------------------------
// Program 5
// Learning to save some things using libfat
//------------------------------------------------------------------------------ 

	
void programs4_5(void)
    {
	/*
	
	SaveData data;
	data.variable = 255;
	data.variable2 = 256;
	
	//write the save data
	FILE* file;
	file = fopen("fat:/savefile.sav", "wb");
	fwrite(&data, 1, sizeof(SaveData),file);
	fclose(file);
	
	//read the save data
	SaveData data2;
	file = fopen("fat:/savefile.sav", "rb");
	fread(&data2,1,sizeof(SaveData),file);
	fclose(file);
	*/
	
	consoleDemoInit();
	WavData16PCM wavdata;
	wavdata.samples = NULL;
	int channel = -1;
	
	if (fatInitDefault()) 
		{
		
		FILE *wavfilePtr = fopen("/menu/audio/express.wav", "rb");
		if (wavfilePtr == NULL)
			{
			iprintf("damn - couldn't open the file\n");
			}
		else
			{
			WavHeader wavfile;
			fread(&wavfile, sizeof(WavHeader), 1, wavfilePtr);
			
			iprintf("Wav file header info:\n");
			iprintf("ChunkID: %.4s\n", wavfile.header.ChunkID);
			iprintf("ChunkSize: %d\n", wavfile.header.ChunkSize);
			iprintf("Format: %.4s\n", wavfile.header.Format);
			
			iprintf("\nWav fmt chunk info:\n");
			iprintf("Subchunk1ID: %.4s\n", wavfile.fmt.Subchunk1ID);
			iprintf("Subchunk1Size: %d\n", wavfile.fmt.Subchunk1Size);
			iprintf("AudioFormat: %d\n", wavfile.fmt.AudioFormat );
			iprintf("NumChannels: %d\n", wavfile.fmt.NumChannels );
			iprintf("SampleRate: %d\n", wavfile.fmt.SampleRate );
			iprintf("ByteRate: %d\n", wavfile.fmt.ByteRate );
			iprintf("BlockAlign: %d\n", wavfile.fmt.BlockAlign );
			iprintf("BitsPerSample: %d\n", wavfile.fmt.BitsPerSample );
			
			iprintf("\nWav data chunk info:\n");
			iprintf("DataChunkID %.4s\n", wavfile.data.DataChunkID);
			iprintf("DataChunkSize %d\n", wavfile.data.DataChunkSize);
			
			//we are assuming 16 bit PCM here..
			//TODO: add something that pads wavdata.samples so that it is 16 byte blocked
			//supposedly looping samples needs padding
			wavdata.samples = (s16*) malloc(wavfile.data.DataChunkSize);
			wavdata.cursor = 0;
			fread(wavdata.samples, 
				  2, //two bytes for s16
				  wavfile.data.DataChunkSize, 
				  wavfilePtr);
			
			
			soundEnable();
			channel = soundPlaySample(wavdata.samples, //location of 16bit unsigned buffer
									SoundFormat_16Bit,  //16 bit PCM
									wavfile.data.DataChunkSize, //size in bytes
									wavfile.fmt.SampleRate, //Hz frequency of sample (i.e., 
									127, //volume 0 to 127 min to max
									64, //pan 0 to 127 left to right
									true, //loop
									0); //loop point
							
			fclose(wavfilePtr);
			
			}
		} 
	else 
		{
		iprintf("fatInitDefault failure: terminating\n");
		}

	while(1) 
		{
		swiWaitForVBlank();
		if (checkExit(TRUE))
			{
			soundDisable();
			if (wavdata.samples != NULL)
				{
				soundKill(channel);
				free(wavdata.samples);
				wavdata.samples = NULL;
				}
			break;
			}
		}
	
 
	}
	