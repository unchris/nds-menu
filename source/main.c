#include "main.h"	
#include "menu.h"
#include "programs1.h"
#include "programs2.h"
#include "programs3.h"
#include "programs4.h"

//---------------------------------------------------------------------------------
int main(void) 
	{
	consoleDemoInit();

	videoSetMode(MODE_FB0);

	vramSetBankA(VRAM_A_LCD);
	
	Menu root, set1, set2, set3, set4;
	InitMenu(&root, "Chris Cameron's DS Menu",NULL, NULL);
	InitMenu(&set1, "Program Set #1", NULL, &root);
	InitMenu(&set2, "Program Set #2", NULL, &root);
	InitMenu(&set3, "Program Set #3", NULL, &root);
	InitMenu(&set4, "Program Set #4", NULL, &root);
	
	Menu set11, set12, set13, set14, set21, set31, set41, set42, set43, set44, set45;
	InitMenu(&set11, "My 'Hello World'", &programs1_1, &set1);
	InitMenu(&set12, "Button States (ARM9)", &programs1_2, &set1);
	InitMenu(&set13, "Button States (ARM9+ARM7)", &programs1_3, &set1);
	InitMenu(&set14, "Frame Buffered Graphics", &programs1_4, &set1);
	
	InitMenu(&set21, "Starfield Game", &programs2_run, &set2);
	
	InitMenu(&set31, "Touchpad Test", &programs3_run, &set3);
	
	InitMenu(&set41, "Mode 5 Test", &programs4_1, &set4);
	InitMenu(&set42, "Mode 5 Main+Sub", &programs4_2, &set4);
	InitMenu(&set43, "256 colour BMP", &programs4_3, &set4);
	InitMenu(&set44, "Simple Tiled BGs", &programs4_4, &set4);
	InitMenu(&set45, "Filesystem Test", &programs4_5, &set4);
	
	Menu* curr = NULL;
	curr = &root;
	
	int index = 0;
	while (1)
		{
		swiWaitForVBlank();
		consoleClear();
		PrintMenu(curr, index);
		scanKeys(); 
		
		
		if (keysDown() & KEY_SELECT)
			{
			iprintf("quitting...");
			DeleteMenu(&root);
			break;
			}
		if (keysDown() & KEY_DOWN)
			{
			//navigate down one menu
			if (index < curr->childCount - 1)
				++index;
			}
		if (keysDown() & KEY_UP)
			{
			//navigate up one menu
			if (index > 0)
				--index;
			}
		if (keysDown() & KEY_A)
			{
			//if the child has a function to run, run it
			//else, navigate current menu to child menu and reset index
			if (curr->children[index]->fPtr != NULL)
				{
				curr->children[index]->fPtr();
				videoSetMode(MODE_FB0);
				vramSetBankA(VRAM_A_LCD);
				consoleDemoInit();
				ClearScreen();
				consoleClear();
				}
			else
				{
				curr = curr->children[index];
				index = 0;
				}	
			}
		if (keysDown() & KEY_B)
			{
			//go back up a menu and reset index
			if (curr->parent != NULL)
				{
				curr = curr->parent;
				index = 0;
				}
				
			}
			
		
		
		}
		
	
	return 0;
	}
