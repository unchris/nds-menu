

/* Menu struct
	-each menu has a title (ds displays max 32 characters wide)
	-each menu has a childCount 
	-each menu has a number of "children" Menus
*/
struct Menu_
	{
	char* title;
	u8 childCount;
	struct Menu_ *parent;
	void (*fPtr)(void);
	struct Menu_ **children;
	
	};
typedef struct Menu_ Menu;

void InitMenu(Menu*, char*, void(*fPtr)(void), Menu*);
void AppendChild(Menu* child, Menu* parent);
void PrintMenu(Menu*, int);

void InitMenu(Menu* menu, char* title, void (*fPtr)(void), Menu* parent)
	{
	menu->title = title;
	menu->fPtr = fPtr;
	menu->childCount = 0;
	menu->children = NULL;
	menu->parent = parent;
	if (parent != NULL)
		AppendChild(menu, parent);
	}
	
void DeleteMenu(Menu* menu)
	{
	int i;
	for (i = 0; i < menu->childCount; ++i)
		{
		DeleteMenu(menu->children[i]);
		}
	free(menu->title);
	menu->title = NULL;
	for (i = 0; i < menu->childCount; ++i)
		{
		free(menu->children[i]);
		}
	menu->children = NULL;
	free(menu);
		
	}
	
void AppendChild(Menu* child, Menu* parent)
	{
	child->parent = parent;
	parent->childCount += 1;
	parent->children = (Menu**) realloc(parent->children, parent->childCount * sizeof(void *));//temp;
	parent->children[parent->childCount - 1] = child;
	}
	
void PrintMenu(Menu* menu, int index)
	{
	iprintf("%s", menu->title);
	iprintf("\n\n");
	int i;
	for (i = 0; i < menu->childCount; ++i)
		{
		if (i == index)
			{
			iprintf("->%d. %s\n", i+1, menu->children[i]->title);
			}
		else
			{
			iprintf("  %d. %s\n", i+1, menu->children[i]->title);
			}
		}
	}