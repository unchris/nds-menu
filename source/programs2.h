#ifndef __PROGRAMS_2_H__
#define __PROGRAMS_2_H__

#define TRUE 1
#define FALSE 0
#define NUM_STARS 40
#define NUM_JUNK 5

#define MIDX 256 >> 1
#define MIDY 192 >> 1
#define FLAME_RIGHT 0
#define FLAME_UP 1
#define FLAME_DOWN 2
 
//void ClearScreen(void);
void programs2_run(void);
 
//------------------------------------------------------------------------------ 
// Star Functions
//------------------------------------------------------------------------------
typedef struct
	{
	int x;
	int y;
	int speed;
	unsigned short colour;
 
	}Star;
	
void InitStars(Star* stars);
void DrawStar(Star* star);
void EraseStar(Star* star);
void MoveStar(Star* star);

//------------------------------------------------------------------------------ 
// Space Ship Functions
//------------------------------------------------------------------------------
typedef struct 
	{
	int x;
	int y;
	int flamePos;
	
	}Ship;

void InitShip(Ship*);
void DrawFlame(Ship*, bool erase);
void MoveShip(Ship*);
void DrawShip(Ship*, bool erase);
	
//------------------------------------------------------------------------------ 
// Space Junk Functions
//------------------------------------------------------------------------------
typedef struct
	{
	int x0;
	int x1;
	int y0;
	int y1;
	u16 colour;
	}SpaceJunk;

void InitOneSpaceJunk(SpaceJunk* junk);
void InitSpaceJunk(SpaceJunk*);
void DrawSpaceJunk(SpaceJunk* junk, bool erase);
void MoveSpaceJunk(SpaceJunk* junk);
void CollisionDetect(Ship* ship, SpaceJunk* junk);

#endif