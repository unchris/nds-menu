/* An adaptation of the dev-scene NDS tutorial Day 3 Starfield Demo */
/* I have added a ship and space junk, with basic movement controls */
/* Hopefully will add basic collision detection */

#ifndef __NDS_HEADER__
#define __NDS_HEADER__
#include <nds.h>
#include <stdio.h>
#endif

#include "programs2.h"
 

 
//------------------------------------------------------------------------------ 
// Star Functions
//------------------------------------------------------------------------------
void InitStars(Star *stars)
{
	int i;
 
	for(i = 0; i < NUM_STARS; i++)
	{
		stars[i].colour = RGB15(31,31,31);
		stars[i].x = rand() % 256;
		stars[i].y = rand() % 192;
		stars[i].speed = rand() % 4 + 1;
	}
}
void DrawStar(Star* star)
{
	VRAM_A[star->x + star->y * SCREEN_WIDTH] = star->colour;
}
 
void EraseStar(Star* star)
{
	VRAM_A[star->x + star->y * SCREEN_WIDTH] = RGB15(0,0,0);
}

void MoveStar(Star* star)
{
	star->x += star->speed;
 
	if(star->x >= SCREEN_WIDTH)
	{
		star->colour = RGB15(31,31,31);
		star->x = 0;
		star->y = rand() % 192;
		star->speed = rand() % 4 + 1;	
	}
}

//------------------------------------------------------------------------------ 
// Space Ship Functions
//------------------------------------------------------------------------------

void InitShip(Ship* ship)
	{
	ship->x = MIDX;
	ship->y = MIDY;
	ship->flamePos = FLAME_RIGHT;
	}
	
void DrawFlame(Ship* ship, bool erase)
	{
	int x0,y0,x1,y1;
	if (ship->flamePos == FLAME_RIGHT)
		{
		x0 = ship->x + 3;
		x1 = ship->x + 7;
		y0 = ship->y-1;
		y1 = ship->y+1;
		
		}
	else if (ship->flamePos == FLAME_UP)
		{
		x0 = ship->x-1;
		x1 = ship->x+1;
		//backwards like this because y0 must be l.t. y1
		y0 = ship->y - 27;
		y1 = ship->y - 3;
		}
	else //FLAME_DOWN
		{
		x0 = ship->x-1;
		x1 = ship->x+1;
		y0 = ship->y + 3;
		y1 = ship->y + 27;
		}
		
	u16 colour;
	if (erase)
		colour =RGB15(0,0,0);
	else
		colour = RGB15(31,0,0);
	int x,y;
	for (x = x0; x <= x1; ++x)
		{
		for (y = y0; y <= y1; ++y)
			{
			VRAM_A[x + y*SCREEN_WIDTH] = colour;
			}
		}
	}
	
void MoveShip(Ship* ship)
	{
	int held = keysHeld();
	if( held & KEY_UP)
		{
		ship->y -= 1;
		if (ship->y < 0)
			ship->y = 0;
		ship->flamePos = FLAME_DOWN;
		}
	else if (held & KEY_DOWN)
		{
		ship->y +=1;
		if (ship->y > SCREEN_HEIGHT)
			ship->y = SCREEN_HEIGHT;
		ship->flamePos=FLAME_UP;
		}
	else
		{
		ship->flamePos=FLAME_RIGHT;
		}
		
	}
	
void DrawShip(Ship* ship, bool erase)
	{
	//draw a green square 5x5 pixels in centre of screen	
	// the x will be from ship->x-2 to ship->x+2
	// the y will be from ship->y-2 to ship->y+2
	int x;
	int y;
	u16 colour;
	if (erase)
		colour =RGB15(0,0,0);
	else
		colour = RGB15(0,31,0);
		
	for (x = ship->x-2; x < ship->x+3; ++x)
		{
		for (y = ship->y-2; y < ship->y+3; ++y)
			{
			VRAM_A[x + y*SCREEN_WIDTH] = colour;
			}
		}
	DrawFlame(ship, erase);
		
	}
	
	
	
//------------------------------------------------------------------------------ 
// Space Junk Functions
//------------------------------------------------------------------------------

void InitOneSpaceJunk(SpaceJunk* junk)
	{
	junk->colour = RGB15(31,31,31);
	junk->x0 = -(rand() % SCREEN_WIDTH); //left of screen
	junk->x1 = junk->x0 + (rand() % 5);
	junk->y0 = rand() % SCREEN_HEIGHT;
	junk->y1 = junk->y0 + (rand() % 25); //could run off screen potentially
	}
void InitSpaceJunk(SpaceJunk* junk)
{
	int i;
	for(i = 0; i < NUM_JUNK; i++)
		InitOneSpaceJunk(&junk[i]);
}

void DrawSpaceJunk(SpaceJunk* junk, bool erase)
	{
	//draw a green square 5x5 pixels in centre of screen	
	// the x will be from ship->x-2 to ship->x+2
	// the y will be from ship->y-2 to ship->y+2
	int x;
	int y;
	u16 colour;
	if (erase)
		colour = RGB15(0,0,0);
	else
		colour = junk->colour;
	
	for (x = junk->x0; x < junk->x1; ++x)
		{
		for (y = junk->y0; y < junk->y1; ++y)
			{
			//it would be better if the offscreen y values were clipped instead
			if (x >= 0 && x < SCREEN_WIDTH && y >= 0 && y < SCREEN_HEIGHT)
				VRAM_A[x + y*SCREEN_WIDTH] = colour;
			}
		}
	}
	
void MoveSpaceJunk(SpaceJunk* junk)
	{
	++junk->x0;
	++junk->x1;	
	if (junk->x0 >= SCREEN_WIDTH)
		{
		InitOneSpaceJunk(junk);
		}
	}

//------------------------------------------------------------------------------ 
// Collision Detection Routine
//------------------------------------------------------------------------------
int points = 0;
void CollisionDetect(Ship* ship, SpaceJunk* junk)
	{
	if (junk->x1 == ship->x - 1)
		{
		if (ship->y > junk->y0 && ship->y < junk->y1)
			{
			junk->colour = RGB15(0,31,0);
			++points;
			}
		}
	}
	
 
//------------------------------------------------------------------------------ 
// Game Loop
//------------------------------------------------------------------------------
void programs2_run(void) 
	{
	int i;
	
	Star stars[NUM_STARS];
	Ship ship;
	SpaceJunk junk[NUM_JUNK];
 
	videoSetMode(MODE_FB0);
	vramSetBankA(VRAM_A_LCD);
 
	consoleDemoInit();
 
    ClearScreen();
	InitStars(stars);
	InitShip(&ship);
	InitSpaceJunk(junk);
 
	while(TRUE)
		{
		consoleClear();
		printf("SCORE: %d", points);
		
		scanKeys();
		swiWaitForVBlank();
		
		for(i = 0; i < NUM_STARS; i++)
			{
			EraseStar(&stars[i]);
			MoveStar(&stars[i]);
			DrawStar(&stars[i]);
			}		
		for(i = 0; i < NUM_JUNK; ++i)
			{
			DrawSpaceJunk(&junk[i], TRUE);
			MoveSpaceJunk(&junk[i]);
			DrawSpaceJunk(&junk[i], FALSE);
			}
		
		DrawShip(&ship, TRUE);
		MoveShip(&ship);
		DrawShip(&ship, FALSE);
		for(i = 0; i < NUM_JUNK; ++i)
			CollisionDetect(&ship, &junk[i]);
		
		if (checkExit(FALSE))
			{
			break;
			}
		}
 
	}